#include <iostream>
#include <stdlib.h>
#include <assert.h>
#include "queue.h"
#include "qtests.h"

using namespace std;

// TODO: testare meglio iteratori. Cast da uno all'altro e gli altri metodi (richiamarli solamente)
// TODO: controllare la chiamata a tutti i metodi
int main(){

	int vals[] = {0, 1, 2, /*5,*/ 8};
	for(int* p = &vals[0]; p != (&vals[3] + 1); p++){

		TEST_DATA_LENGTH = *p;

		cout << "!*!*! TEST_DATA_LENGTH = " << TEST_DATA_LENGTH << " !*!*!"<< endl;
		if(QUEUE_TEST){
		println("[TEST] QUEUE & DEQUEUE");
		testQueueDequeue(chars); testQueueDequeue(ints); testQueueDequeue(strings);}

	    if(ITER_TEST){
	    println("\n[TEST] ITERATORS");
	    testIterator(chars); testIterator(ints); testIterator(strings);}

	    if(CTOR_TEST){
	    println("\n[TEST] CTORS & OPERATOR=");
		testCtorsOperator(chars);testCtorsOperator(ints);testCtorsOperator(strings);
		testTemplateCtors(chars, ints); testTemplateCtors(charints, chars);}
		
		if(SPEC_TEST){
		println("\n[TEST] SPECIAL TESTS");	
		testTransformIf();}
	}
	if(GETSET_TEST){
	println("\n[TEST] SETTERS & GETTERS");
	testSetGet(chars); testSetGet(ints); testSetGet(strings);}

	if(SPEC_TEST){
	println("\n[TEST] SPEC TEST 2");
	testStressTest<int>(); testQueueOfQueue();}

	testConstness(strings);

	return 0;
}

template <typename T> void testConstness(T test_data[]){
	Queue<T> myQ; //= new Queue<T>();
	
	for (int i = 0; i < TEST_DATA_LENGTH; ++i){
		myQ.enqueue(test_data[i]);
	}

	const Queue<T>* myCQ = new Queue<T>(myQ);
	cout << *myCQ << endl;

	typename Queue<T>::iterator i = myQ.begin(), i2;
	typename Queue<T>::const_iterator ci(i), ci2;
	ci = i;
	i = i;
	i2 = i;
	ci2 = ci;
	// i->clear(); solo per il tipo string
	// cout << *myCQ << endl;

	delete myCQ;
}

void testQueueOfQueue(){
	const int SIZE = 300;
	println("!*!*! QUEUE of QUEUES !*!*!");
	Queue<int>** myQs = new Queue<int>*[5]; 
//  L'inizializzazione 'automatica' è relativa solo all'array di puntatori, non agli oggetti puntati
//  Vengono quindi inizializzati solo i 5 puntatori
	myQs[0] = new Queue<int>();		myQs[1] = new Queue<int>();
	myQs[2] = new Queue<int>();		myQs[3] = new Queue<int>();
	myQs[4] = new Queue<int>();

	println("Creating a Queue<Queue<T> >");
	Queue<Queue<int> >* queueOfQueue = new Queue<Queue<int> >;
	for (int i = 0; i < SIZE; ++i){
		myQs[0]->enqueue(rand() % 256);
		myQs[1]->enqueue(rand() % 256);
		myQs[2]->enqueue(rand() % 256);
		myQs[3]->enqueue(rand() % 256);
		myQs[4]->enqueue(rand() % 256);
	}
	println("Enqueuing queues");
	queueOfQueue->enqueue(*myQs[0]); queueOfQueue->enqueue(*myQs[1]);
	queueOfQueue->enqueue(*myQs[2]); queueOfQueue->enqueue(*myQs[3]);
	queueOfQueue->enqueue(*myQs[4]);

	//println("!*!*!*!*!*! Printing  queueOfQueue !*!*!*!*!*!");
	// cout << *queueOfQueue << endl; // troppo lungo l'output!!!
	// assertions
	Queue<int>* tmp = new Queue<int>();
	Queue<int>::iterator it, it2;
	int j = 0;
	while(queueOfQueue->dequeue(*tmp)){
		it = tmp->begin();
		it2 = myQs[j++]->begin();
		for (int i = 0; i < SIZE; ++i){
			assert(&*it != &*it2); // si assicura che siano in una locazione diversa
			assert(*(it++) == *(it2++)); // si assicura che siano uguali
		}
	}

	// Dealloca tutto
	for (int i = 0; i < 5; ++i)
		delete myQs[i];
	delete[] myQs;
	delete tmp;
	delete queueOfQueue;
}

template <typename T> void testStressTest(){
	const int SIZE = 300;
	srand(time(0));
	println("Creating 300 elements of type T");
	T* data = new T[SIZE];
	println("Inserting in queue");
	Queue<T>* myQ = new Queue<T>();
	for (int i = 0; i < SIZE; ++i){
		data[i] = rand() % 256;
		myQ->enqueue(data[i]);
	}
	// assertions
	typename Queue<T>::const_iterator i = myQ->begin(),
	e = myQ->end(); 
	int j = 0;
	while(i != e){
		assert(&*i != &data[j]);     // controlla che siano in posizione di memoria diversa	
		assert(*(i++) == data[j++]);  // ma con lo stesso valore
	}

	//println("All elements are equal and in different location");
	// cout << *myQ << endl;

	delete myQ;
	delete[] data; 
}

void testTransformIf(){
	Queue<int>* myQ = new Queue<int>();
	for (int i = 0; i < TEST_DATA_LENGTH; ++i){
		myQ->enqueue(ints[i]);
	}
	println("If even(), mod()");
	transformif(*myQ, even(), mod());
	cout << *myQ << endl;

	// Controllo che l'inserimento sia corretto
	typename Queue<int>::iterator i = myQ->begin(), end = myQ->end();
	int j = 0;
	mod m; even e;
	while(i != end){
		assert(&*i != &ints[j]);
		assert(*i == (e(ints[j]) ? m(ints[j++]) : ints[j++])); 
		i++;
	}

	delete myQ;

	Queue<string>* myQs = new Queue<string>();
	for (int k = 0; k < TEST_DATA_LENGTH; ++k){
		myQs->enqueue(strings[k]);
	}

	println("If isLongString(), doubleString()");
	transformif(*myQs, isLongString(), doubleString());
	cout << *myQs << endl;
	delete myQs;
}

template <typename T, typename K> 
void testTemplateCtors(T test_data[], K test_data1[]){
	Queue<T>* myQT;
	Queue<K>* myQK = new Queue<K>();
	for (int i = 0; i < TEST_DATA_LENGTH; ++i){
		myQK->enqueue(test_data1[i]);
	}
	println("Copy Ctor and operator= from Queue<K> to Queue<T>");
	myQT = new Queue<T>(*myQK);

	typename Queue<T>::iterator i = myQT->begin(), e = myQT->end();
	typename Queue<K>::iterator i2 = myQK->begin(), e2 = myQK->end();
	while(i != e){
		assert(&*i != (T*)&*i2);
		assert(*(i++) == static_cast<T>(*(i2++))); 
	}
	cout << *myQT << endl;
	*myQK = *myQT;
	
	i = myQT->begin(); 	i2 = myQK->begin();
	e = myQT->end();	e2 = myQK->end();
	// Assertions
	while(i != e){
		assert(&*i != (T*)&*i2);
		assert(*(i++) == static_cast<T>(*(i2++))); 
	}

	delete myQT;
	delete myQK;

	myQT = new Queue<T>();
	for (int i = 0; i < TEST_DATA_LENGTH; ++i){
		myQT->enqueue(test_data[i]);
	}
	
	println("Copy Ctor and operator= from Queue<T> to Queue<K>");
	myQK = new Queue<K>(*myQT);
	
	i = myQT->begin(); 	i2 = myQK->begin();
	e = myQT->end();	e2 = myQK->end();
	// Assertions
	while(i != e){
		assert(&*i != (T*)&*i2); // Controlla che siano distinti
		assert(*(i++) == static_cast<T>(*(i2++))); // ma uguali
	}
		
	*myQK = *myQT; 

	i = myQT->begin(); 	i2 = myQK->begin();
	e = myQT->end();	e2 = myQK->end();
	// Assertions
	while(i != e){
		assert(&*i != (T*)&*i2); // Controlla che siano distinti
		assert(*(i++) == static_cast<T>(*(i2++))); // ma uguali
	}
	cout << *myQT << endl;
	delete myQT;
	delete myQK;
}

template<typename T> void testCtorsOperator(T test_data[]){
	println("Allocating on stack");
	Queue<T> myQs;
	for (int i = 0; i < TEST_DATA_LENGTH; ++i){
		myQs.enqueue(test_data[i]);
	}
	cout << myQs << endl;

	println("Copy Ctor from stack queue: ");
	Queue<T>* myQ = new Queue<T>(myQs);
	
	typename Queue<T>::const_iterator i = myQ->begin(),
	e = myQ->end(), i2 = myQs.begin(), e2 = myQs.end();
	while(i != e){
		assert(&*i != &*i2);		// Si assicura che l'elemento sia diverso
		assert(*(i++) == *(i2++)); 	// ma che sia una copia
	}

	println("Copying with operator=, should be equal to others");
	Queue<T>* myQ2 = new Queue<T>();
	*myQ2 = *myQ;

	// assertions
	i = myQ->begin(); e = myQ->end();
	i2 = myQ2->begin(); e2 = myQ2->end();
	while(i != e){
		assert(&*i != &*i2);		// Si assicura che l'elemento sia diverso
		assert(*(i++) == *(i2++)); 	// ma che sia una copia
	}

	delete myQ;
	cout << *myQ2 << endl;;
	delete myQ2;
}

template<typename T> void testIterator(T test_data[]){
	Queue<T>* myQ = new Queue<T>();
	for (int i = 0; i < TEST_DATA_LENGTH; ++i){
		myQ->enqueue(test_data[i]);
	}

	typename Queue<T>::iterator i = myQ->begin();
	typename Queue<T>::iterator e = myQ->end();

	println("Printing queue using iterator");
	for (;i != e; i++){
		cout << *i << '\t';
	}
	println("");

	println("Printing queue with operator<< and const_iterator");
	cout << *myQ << endl;
	delete myQ;
}

template<typename T> void testQueueDequeue(T test_data[]){
	Queue<T>* myQ = new Queue<T>();
	for (int i = 0; i < TEST_DATA_LENGTH; ++i){
		myQ->enqueue(test_data[i]);
	}
	println("Dequeue of the elements");
	T* out = new T;
	while(myQ->dequeue(*out)){
		cout << *out << "\t";
	}

	println("");

	delete out;
	delete myQ;

	myQ = new Queue<T>();
	for (int i = 0; i < TEST_DATA_LENGTH; ++i){
		myQ->enqueue(test_data[i]);
	}
	while(myQ->dequeue()){}

	println("");
	delete myQ;
}

template <typename T> void testSetGet(T test_data[]){
	Queue<T>* myQ = new Queue<T>();
	myQ->enqueue(test_data[0]);
	myQ->enqueue(test_data[7]);
	cout << "First should be:\t" << test_data[0] << endl;
	cout << "Last should be: \t" << test_data[7] << endl;

	print("Reading first:\t\t");
	T& first = myQ->getFirst();
	println(first);
	print("Writing on first:\t");
	first = test_data[2];
	println(myQ->getFirst());

	print("Reading last:\t\t");
	T& last = myQ->getLast();
	println(last);
	print("Writing on last:\t");
	last = test_data[3];
	println(myQ->getLast());

	println("Copy ctor to const");
	const Queue<T> cMyQ = *myQ;
	print("Reading first const:\t");
	const T& cfirst = cMyQ.getFirst();
	println(cfirst);
	print("Reading last const:\t");
	const T& clast = cMyQ.getLast();
	println(clast);

	assert(cfirst == test_data[2]);
	assert(clast == test_data[3]);
	println("");
	delete myQ;
}