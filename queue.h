#ifndef QUEUE_H
#define QUEUE_H
#include <iostream>
#include "node.h"

using namespace std;

typedef unsigned int uint;

/**
* Coda FIFO templata.
*/
template <typename T> class Queue{
private:
	node<T>* _head; // primo elemento "da servire", primo arrivato
	node<T>* _tail; // ultimo elemento "da servire", ultimo arrivato
	uint _length;

public:
	class iterator;
	class const_iterator;
	/**
	* Costruisce una coda vuota.
	*/
	Queue() : _head(0), _tail(0), _length(0){}
	
	Queue(const Queue<T>& other) : _head(0), _tail(0), _length(0) {
		if(other.length() > 0){
			typename Queue<T>::const_iterator i = other.begin();
			typename Queue<T>::const_iterator e = other.end();
			enqueue(i, e); // gestione eccezioni all'interno
		}
	}

	/**
	* Costruisce una coda copiandone un'altra.
	* \param other coda da cui copiare gli elementi.
	*/
	template <typename K> Queue(const Queue<K>& other) : _head(0), _tail(0), _length(0) {
		if(other.length() > 0){
			typename Queue<K>::const_iterator i = other.begin();
			typename Queue<K>::const_iterator e = other.end();
			enqueue<K>(i, e); // gestione eccezioni all'interno
		}
	}
	
	/**
	* Overload di operator=. Fa una copia della coda passata come parametro
	* \param rhs Queue da copiare in this
	*/
	Queue<T>& operator=(const Queue<T>& rhs){
		if(&rhs != this){
			Queue<T> tmp(rhs); // gestione memoria insufficiente nel copyctor
			this->swap(tmp);
		}
		return *this;
	}
	
	/**
	* Overload di operator=. Fa una copia della coda passata come parametro
	* \param rhs Queue da copiare in this
	*/
	template <typename K> Queue<T>& operator=(const Queue<K>& rhs){
		if((Queue<T>*)&rhs != this){
			Queue<T> tmp(rhs); // gestione memoria insufficiente nel copyctor
			this->swap(tmp);
		}
		return *this;
	}
	

	/**
	* Non richiamare esplicitamente! Usare clear().
	*/
	~Queue(){ clear(); }

	/**
	* Accoda un elemento in coda. Non inserisce elementi NULL
	* \param item elemento da aggiungere in coda. Se NULL non viene inserito.
	*/
	void enqueue(const T& item){
		node<T>* newItem = new node<T>(item, 0); 
		if(_length == 0){
			_head = newItem;
			_tail = newItem;
		}else{
			_tail->setNext(newItem);
			_tail = newItem;
		}
		_length++;
	}

	/**
	* Accoda gli elementi appartenenti a un sottoinsieme ad un'altra coda. Estremi inclusi.
	* \param begin Indica l'inizio della coda da cui copiare elementi.
	* \param end Indica l'ultimo elemento da accodare.
	*/
	void enqueue(typename Queue<T>::const_iterator& begin, 
				 typename Queue<T>::const_iterator& end){
		try{
			if(begin != end){
				enqueue(*begin);
				begin++;
				enqueue(begin, end);
			}
		} catch(...){clear(); throw;}
	}
	
	/**
	* Versione con static_cast degli elementi da K a T. Estremi inclusi.
	* \param begin Indica l'elemento di tipo K a inizio della coda da cui copiare elementi.
	* \param end Indica l'ultimo elemento di tipo K da accodare.
	*/
	template <typename K> 
	void enqueue(typename Queue<K>::const_iterator& begin, 
				 typename Queue<K>::const_iterator& end){
		try{
			if(begin != end){
				enqueue(static_cast<T>(*begin));
				begin++;
				enqueue<K>(begin, end);
			}
		} catch(...){clear(); throw;}
	}
	
	/**
	* Rimuove e restituisce il primo elemento dalla coda.
	* \param out Locazione in memoria su cui scrivere l'elemento estratto. Deve essere già inizializzata.
	* /return True se l'elemento è stato estratto, False se non ci sono elementi da estrarre.
	*/
	bool dequeue(T& out){
		if(_length < 1)
			return false;

		out = T(_head->getItem()); // salva l'elemento nella locazione di memoria indicata

		node<T>* tmp = _head;
		_head = _head->getNext();
		tmp->setNext(0); // si assicura che l'elemento non punti più a nessuna area di memoria
		delete tmp;
		tmp = 0;
		if(_length == 1){
			_tail = 0;
		} 
		_length--;
		return true;
	}

	/**
	* Rimuove senza restituire il primo elemento dalla coda.
	* /return True se l'elemento è stato estratto, False se non ci sono elementi da estrarre.
	*/
	bool dequeue(){
		if(_length < 1)
			return false;

		node<T>* tmp = _head;
		_head = _head->getNext();
		tmp->setNext(0);
		delete tmp; 
		tmp = 0;
		if(_length == 1){
			_tail = 0;
		}
		_length--;
		return true;
	}

	/**
	* Ritorna il primo elemento della coda.
	* /return Il primo elemento della coda.
	*/
	T& getFirst(){
		return _head->getItem();
	}
	
	/**
	* Ritorna l' ultimo elemento della coda.
	* /return L' ultimo elemento della coda.
	*/
	T& getLast(){
		return _tail->getItem();
	}
	
	/**
	* Ritorna il primo elemento della coda.
	* /return Il primo elemento della coda in sola lettura.
	*/
	const T& getFirst() const {
		return _head->getItem();
	}
	
	/**
	* Ritorna l' ultimo elemento della coda.
	* /return L' ultimo elemento della coda in sola lettura.
	*/
	const T& getLast() const {
		return _tail->getItem();
	}

	/**
	* Ritorna il numero di elementi presenti in coda.
	* /return Il numero di elementi presenti in coda.
	*/
	unsigned int length() const {
		return _length; 
	}
	
	/**
	* Pulisca la memoria cancellando tutti i nodi della coda.
	*/
	void clear(){
		node<T>* tmp;
		while(_length > 0){
			tmp = _head;
			_head = _head->getNext();
			delete tmp;
			_length--;
		}
		_head = 0;
		_tail = 0;
		_length = 0;
		tmp   = 0;
	}
	
	/**
	* Iteratore per il tipo Queue<T>
	*/
	class iterator {
        node<T>* n;
    public:
        typedef std::forward_iterator_tag iterator_category;
        typedef T 			              value_type;
        typedef ptrdiff_t                 difference_type;
        typedef T*			              pointer;
        typedef T&			              reference;

        iterator() : n(0) {}

        iterator(const iterator& other) : n(other.n) {}

        ~iterator(){ n = 0; }

        iterator& operator=(const iterator& other){
        	if (this != &other){
        		iterator tmp(other);
        		std::swap(n, tmp.n);
        	}
        	return *this;
        }

        /**
        * Overload di *.
        * \return Ritorna l'elemento corrente.
        */
        reference operator*() const {
            return n->getItem();
        }
      	/**
        * Overload di ->.
        * \return Un puntatore all'elemento corrente.
        */
        pointer operator->() const {
            return &(n->getItem());
        }
        
        /**
        * Post-incremento.
        * \return Una copia dell'iteratore non incrementata.
        */
        iterator operator++(int){ // post incremento
            iterator temp = *this;
            n = n->getNext();
            return temp;
        }
        
        /**
        * Pre-incremento
        * \return L'iteratore incrementato.
        */
        iterator operator++(){    // pre incremento
            n = n->getNext();
            return *this;
        } 
        /**
        * Confronta se due iteratori puntano allo stesso oggetto.
        * \param other Iteratore con cui fare il confronto.
        * \return True se puntano allo stesso oggetto. False altrimenti.
        */
        bool operator==(const iterator& other) const{
            return n == other.n;
        }
        
        /**
        * Confronta se due iteratori non puntano allo stesso oggetto.
        * \param other Iteratore con cui fare il confronto.
        * \return True se non puntano allo stesso oggetto. False altrimenti.
        */
        bool operator!=(const iterator& other) const{
            return n != other.n;
        }
       	//class const_iterator;
        friend class const_iterator;
        /**
        * Confronta se due iteratori puntano allo stesso oggetto.
        * \param other Iteratore costante con cui fare il confronto.
        * \return True se puntano allo stesso oggetto. False altrimenti.
        */
        bool operator==(const const_iterator& other) const{
            return n == other.n;
        }
        
        /**
        * Confronta se due iteratori non puntano allo stesso oggetto.
        * \param other Iteratore costante con cui fare il confronto.
        * \return True se non puntano allo stesso oggetto. False altrimenti.
        */
        bool operator!=(const const_iterator& other) const{
            return n != other.n;
        }
   	private:
        friend class Queue;
        /**
        * Istanzia un iterator con inizio su a.
        * \param a Nodo iniziale.
        */
        iterator(node<T>* a) : n(a){}
    };

    /**
	* Versione const dell'iteratore per il tipo Queue<T>
	*/
    class const_iterator {
    	const node<T>* n;  
    public:
        typedef std::forward_iterator_tag iterator_category;
        typedef const T          value_type;
        typedef ptrdiff_t	     difference_type;
        typedef const T*         pointer;
        typedef const T&         reference;
        /**
        * Istanzia un const_iterator con nodo iniziale 0.
        */
        const_iterator(): n(0){}
        /**
        * Istanzia un const_iterator con nodo iniziale di other.
        * \param other Const_iterator da copiare.
        */
        const_iterator(const const_iterator &other): n(other.n) {}
        /**
        * Istanzia un iterator con nodo iniziale di other.
        * \param other iterator da copiare.
        */
        const_iterator(const iterator &other): n(other.n) {}

        ~const_iterator(){ n = 0; }
        /**
        * Overload di =.
        */
        const_iterator& operator=(const const_iterator &other) {
            if(&other != this){
                const_iterator tmp(other);
                std::swap(n, tmp.n);
            }
            return *this;
        }

        /**
        * Overload di *.
        * \return Ritorna l'elemento corrente.
        */
        reference operator*() const {
            return n->getItem();
        }

        /**
        * Overload di ->.
        * \return Un puntatore all'elemento corrente.
        */
        pointer operator->() const {
            return &(n->getItem());
        }

        /**
        * Post-incremento.
        * \return Una copia dell'iteratore non incrementata.
        */
        const_iterator operator++(int) {
            const_iterator tmp(*this);
            n = n->getNext();
            return tmp;
        }

        /**
        * Pre-incremento.
        * \return Una copia dell'iteratore non incrementata.
        */
        const_iterator operator++() {
            n = n->getNext();
            return *this;
        }

        /**
        * Confronta se due iteratori puntano allo stesso oggetto.
        * \param other Iteratore costante con cui fare il confronto.
        * \return True se puntano allo stesso oggetto. False altrimenti.
        */
        bool operator==(const const_iterator &other) const {
            return n == other.n;
        }        

        /**
        * Confronta se due iteratori non puntano allo stesso oggetto.
        * \param other Iteratore costante con cui fare il confronto.
        * \return True se non puntano allo stesso oggetto. False altrimenti.
        */
        bool operator!=(const const_iterator &other) const {
            return n != other.n;
        }

        friend class iterator;

        /**
        * Confronta se due iteratori puntano allo stesso oggetto.
        * \param other Iteratore con cui fare il confronto.
        * \return True se puntano allo stesso oggetto. False altrimenti.
        */
        bool operator==(const iterator &other) const {
            return n == other.n;
        }        

        /**
        * Confronta se due iteratori non puntano allo stesso oggetto.
        * \param other Iteratore con cui fare il confronto.
        * \return True se non puntano allo stesso oggetto. False altrimenti.
        */
        bool operator!=(const iterator &other) const {
            return n != other.n;
        }
    private:
        friend class Queue;
        /**
        * Istanzia un const_iterator con inizio su a.
        * \param a Nodo iniziale.
        */
        const_iterator(node<T>* a) : n(a) { }
    };// fine di const_iterator

    /**
    * Ritorna un iteratore impostato al primo elemento della coda.
    * \return L'iteratore sulla coda impostato sul primo elemento.
    */
    iterator begin() { return iterator(_head); }
    /**
    * Ritorna un iteratore impostato sull'ultimo elemento della coda (0).
    * \return L'elemento a fine collezione (0).
    */
    iterator end() { return iterator(_tail == 0 ? 0 : _tail->getNext()); }

    /**
    * Ritorna un iteratore impostato al primo elemento della coda.
    * \return L'iteratore sulla coda impostato sul primo elemento.
    */
    const_iterator begin() const { return const_iterator(_head); }
    /**
    * Ritorna un iteratore impostato sull'ultimo elemento della coda (0).
    * \return L'elemento a fine collezione (0).
    */
    const_iterator end() const { return const_iterator(_tail == 0 ? 0 : _tail->getNext()); }    

private: 
	void swap(Queue<T>& tmp){
		std::swap(_head, tmp._head);
		std::swap(_tail, tmp._tail);
		std::swap(_length, tmp._length);
	}
};

/**
* Per ogni elemento di 'q', se 'pred', applica 'op'.
* \param q Coda su cui applica 'op' se 'pred'
* \param pred Predicato che determina su quali elementi applicare 'q'
* \param op Operazione da applicare all'elemento per cui vale 'pred'
* /return il numero di elementi per cui vale 'pred' ed è stato applicato 'op'
*/
template <typename T, typename	P, typename F> 
int transformif(Queue<T>& q, P pred, F op){
	typename Queue<T>::iterator i = q.begin(), e = q.end();
	int count = 0;
	while(i != e){
		if(pred(*i)){
			*i = op(*i);
			count++;
		}
		i++;
	}
	return count;
}

/**
* Overload di operator<< per gli oggetti di tipo Queue<T>. Stampa gli elementi e la lunghezza.
*/
template<typename T> ostream& operator<<(ostream& out, const Queue<T>& q){
	typename Queue<T>::const_iterator i = q.begin();
	typename Queue<T>::const_iterator e = q.end();
	int n = 1; char separator;
	while(i != e){
		separator = n++ % 8 == 0 ? '\n' : '\t';
		out << *(i++) << separator;
	}
	cout << (--n % 8 == 0 ? "" : "\n") << "length = " << q.length() << endl;
	return out;
}

#endif