
template <class T>
struct node{

private:
	T item;
	node<T>* next;
	
public:

	/**
	* Costruisce un nuovo nodo da un nodo esistente.
	* \param n nodo esistente da cui creare il nuovo nodo
	*/
	node(node<T>& n){
		setItem(n.getItem());
		setNext(n.getNext());
	}
	
	/**
	* Costruisce un nuovo nodo.
	* \param i oggetto da contenere
	* \param n nodo successivo
	*/
	node(const T& i, node<T>* n){
		setItem(i);
		setNext(n);
	}

	/**
	* Restituisce un reference all'oggetto contenuto nel nodo.
	* \return reference all'oggetto contenuto.
	*/
	T& getItem(){
		return item;
	}
	
	/**
	* Restituisce un puntatore al nodo successivo.
	* \return puntatore al nodo successivo. E' NULL in caso non ci sia nessun nodo successivo
	*/
	node<T>* getNext(){
		return next;
	}

	/**
	* Versione costante che restituisce un reference all'oggetto contenuto nel nodo.
	* \return reference costante all'oggetto contenuto.
	*/
	const T& getItem() const{
		return item;
	}
	
	/**
	* Restituisce un puntatore al nodo costante successivo.
	* \return puntatore al nodo costante successivo. NULL in caso non ci sia nessun nodo successivo
	*/
	const node<T>* getNext() const{
		return next;
	}

	/**
	* Imposta l'oggetto memorizzato nel nodo. Si consiglia l'overload di operator= per il tipo T
	* \param i oggetto da memorizzare.
	*/
	void setItem(const T& i){
		item = i; // l'elemento viene copiato nella memoria riservata a node
	}

	/**
	* Imposta il puntatore al nodo successivo.
	* \param n puntatore al nodo successivo. Può essere NULL.
	*/
	void setNext(node<T>* n){
		next = n;
	}
};