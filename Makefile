all:
	g++ -g main.cpp -o q
r:
	# Compila ed esegui
	g++ -g main.cpp -o q 
	./q 				 
v:
	# Compila ed esegui con valgrind
	g++ -g main.cpp -o q 
	valgrind --leak-check=full ./q		 
d:	
	# Compila ed esegui il debugger
	g++ -g main.cpp -o q 
	gdb -q ./q
