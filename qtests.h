#ifndef QTESTS_H
#define QTESTS_H
#include <string>
using namespace std;

int TEST_DATA_LENGTH = 8; // Default Value

char 	chars[]		= {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
int  	ints[]		= {42, -1, 23, -84, 678, -99999, 55, 0};
string 	strings[]  	= {"AA", "BB? -B", "{}", "C", "T3ST", "__", "tab", "last"};
int 	charints[]	= { 72, 101, 108, 108, 111, 67, 43, 43 };

// Triggers
const bool QUEUE_TEST  = true; // Abilita i test per queue ed enqueue
const bool GETSET_TEST = true; // Abilita i test per i getter e i setter
const bool ITER_TEST   = true; // Abilita i test per gli iteratori
const bool CTOR_TEST   = true; // Abilita i test per i costruttori
const bool SPEC_TEST   = true; // Abilita dei test speciali

// Funzioni di test

/**
* Funzione per testare le versioni template dei costruttori.
*/
template <typename T, typename K> void testTemplateCtors(T test_data[], K test_data1[]);
/**
* Funzione per testare operator= e i costruttori non templati.
*/
template <typename T> void testCtorsOperator(T test_data[]);
/**
* Funzione per testare i metodi enqueue() e dequeue().
*/
template <typename T> void testQueueDequeue(T test_data[]);
/**
* Funzione per testare i getter ed i setter e l'accessibilità che forniscono.
*/
template <typename T> void testSetGet(T test_data[]);
/**
* Funzione per testare gli iteratori.
*/
template <typename T> void testIterator(T test_data[]);
/**
* Funzione per testare il comportamento con quantità di dati più grandi
*/
template <typename T> void testStressTest();
/**
* Funzione per testare la funzione transformIf()
*/
void testTransformIf();
/**
* Funzione per testare il comportamento di Queue<T> quando memorizza altre Queue<T>
*/
void testQueueOfQueue();
template <typename T> void testConstness(T test_data[]);

// Predicates
struct even{ bool operator()(int num){
	return num % 2 == 0; }};
struct isLongString{ bool operator()(string& s){
	return s.size() < 4; }};
// Operators
struct adder{ int operator()(int& num){
	return ++num; }};
struct mod{	int operator()(int num){
	return num < 0 ? num * -1 : num; }};
struct doubleString{ string& operator()(string& s){
	s += s; return s; }};

// Utility
template <typename T> void print(T s){cout << s;}
template <typename T> void println(T s){print(s);print("\n");}


#endif